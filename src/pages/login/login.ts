import { Component } from '@angular/core';
import { AlertController, IonicPage, Loading, LoadingController, NavController } from 'ionic-angular';
import { ApiUocProvider } from "../../providers/api-uoc/api-uoc";

@IonicPage()
@Component({
  selector: 'page-login',
  templateUrl: 'login.html'
})

export class LoginPage {

  loading: Loading;
  waitingMsgLogin = "Login with your account ...";
  loginCredentials = {email: '', password: ''};
  authorName = "Pere Bohigas Boladeras";

  constructor(public navCtrl: NavController, public apiUOC: ApiUocProvider, private loadingCtrl: LoadingController, private alertCtrl: AlertController) {
  }

  login() {

    console.log(this.waitingMsgLogin);
    this.showLoading();

    this.apiUOC.login(this.loginCredentials.email, this.loginCredentials.password).subscribe(
      data => {

        if (data.status == "OK") {
          this.navCtrl.setRoot('MainPage', {userId: data.id});
        } else {
          this.showError(data.message);
        }

      },
      err => {
        console.log("Error on the request")
      }
    );
  }

  showLoading() {
    this.loading = this.loadingCtrl.create({
      content: this.waitingMsgLogin,
      dismissOnPageChange: true
    });
    this.loading.present();
  }

  showError(text) {
    this.loading.dismiss();

    let alert = this.alertCtrl.create({
      title: "Acces denied",
      subTitle: text,
      buttons: ["OK"]
    });
    alert.present();
  }

}
