import { Component } from '@angular/core';
import {AlertController, IonicPage, Loading, LoadingController, NavController, NavParams} from 'ionic-angular';
import { Geolocation } from "@ionic-native/geolocation";
import { ApiUocProvider } from "../../providers/api-uoc/api-uoc";

@IonicPage()
@Component({
  selector: 'page-add-sighting',
  templateUrl: 'add-sighting.html',
})

export class AddSightingPage {

  birdId: number;
  birdName: string;
  latitude: number;
  longitude: number;
  placeDescription: string;
  loading: Loading;
  sendingSightingMsg = "Sending the sighting's data ...";

  constructor(public navCtrl: NavController, public navParams: NavParams, private geolocation: Geolocation, public apiUOC: ApiUocProvider, private loadingCtrl: LoadingController, private alertCtrl: AlertController) {
    this.birdId = navParams.get('birdId');
    this.birdName = navParams.get('birdName');
  }

  ionViewDidLoad() {
    this.getGeolocation();
    console.log("Show AddSighting page");
  }

  addSighting() {

      console.log(this.sendingSightingMsg);
      this.showLoading();

      console.log(this.birdId, this.placeDescription, this.latitude, this.longitude);

      this.apiUOC.addSighting(this.birdId, this.placeDescription, this.latitude, this.longitude).subscribe(
        data => {
          console.log("Request status: " + data.status);
          if (data.status == "OK") {
            this.loading.dismiss();
            this.navCtrl.pop();
          } else {
            this.showError(data.message);
          }

        },
        err => {
          console.log("Error on the request")
        }
      );
    }

  showLoading() {
    this.loading = this.loadingCtrl.create({
      content: this.sendingSightingMsg,
      dismissOnPageChange: true
    });
    this.loading.present();
  }

  showError(text) {
    this.loading.dismiss();

    let alert = this.alertCtrl.create({
      title: "Error",
      subTitle: text,
      buttons: ["Try again"]
    });
    alert.present();
  }

  getGeolocation() {
    this.geolocation.getCurrentPosition().then((resp) => {
      this.latitude = resp.coords.latitude;
      console.log("Get latitude: " + this.latitude);
      this.longitude = resp.coords.longitude;
      console.log("Get latitude: " + this.latitude);

    }).catch((error) => {
      console.log("Error getting location", error)
    });
  }

}
