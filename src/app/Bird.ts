export class Bird {

  id: number;
  name: string;
  imageSrc: string;
  globalSightings: number;
  userSightings: number;
  description: string;

  constructor(id, name, imageSrc, globalSightings, userSightings?){

    this.id = id;
    this.name = name;
    this.imageSrc = imageSrc;
    this.globalSightings = globalSightings;
    if (userSightings != null) {
      this.userSightings = userSightings;
    } else {
      this.userSightings = 0;
    }
    this.description = "";

  }

}
