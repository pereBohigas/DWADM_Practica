import {Component} from '@angular/core';
import {IonicPage, Loading, LoadingController, NavController, NavParams} from 'ionic-angular';
import {ApiUocProvider} from "../../providers/api-uoc/api-uoc";
import {Sighting} from "../../app/Sighting";

@IonicPage()
@Component({
  selector: 'page-bird-details',
  templateUrl: 'bird-details.html',
})
export class BirdDetailsPage {

  private loading: Loading;
  private waitingMsgBirdDetails = "Loading bird details ...";

  Id: number;
  Name: string;
  ImageSrc: string;
  Description: string;
  GlobalSightings: number;
  ListSightings: Array<Sighting> = [];

  constructor(public navCtrl: NavController, public navParams: NavParams, private loadingCtrl: LoadingController, public apiUOC: ApiUocProvider) {
    this.Id = navParams.get('idBird');
  }

  ionViewDidLoad() {
    this.loadBirdDetails();
  }

  loadBirdDetails() {

    console.log(this.waitingMsgBirdDetails);
    this.showLoading();

    this.apiUOC.getBirdDetails(this.Id)
      .subscribe(
        data => {

          this.Name = data[0].bird_name;
          this.ImageSrc = data[0].bird_image;
          this.Description = data[0].bird_description;
          this.GlobalSightings = data[0].bird_sightings;

          data[0].sightings_list.forEach(s => {
            var loadSighting = new Sighting(s.id, s.idAve, s.place, s.long, s.lat);
            this.ListSightings.push(loadSighting);
          });

          console.log("*******************************");
          console.log("Details from the selected bird:");
          console.log("\tId: " + this.Id.toString());
          console.log("\tName: " + this.Name.toString());
          console.log("\tImage's URL: " + this.ImageSrc.toString());
          console.log("\tDescription: " + this.Description.toString());
          console.log("\tGlobal sightings: " + this.GlobalSightings.toString());
        },
        err => {
          console.log("Error on the request")
        }
      );
    this.loading.dismiss();
  }

  private showLoading() {
    this.loading = this.loadingCtrl.create({
      content: this.waitingMsgBirdDetails,
      dismissOnPageChange: true
    });
    this.loading.present();

  }

  addSighting() {
    this.navCtrl.push('AddSightingPage', {birdId: this.Id, birdName: this.Name});
  }

}
